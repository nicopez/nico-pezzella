﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace TextAdventure
{
    public class Game
    {
        private String gameName = "";   // a String initialized to empty
        private bool shouldExit = false; // Create a bool (true/false logical operator) indicating if we should exit the game
        private int die = 0;
        private int dietwo = 0;
        private int diethree = 0;
        private Random rand = new Random();
        private int score = 0;
        private int balance = 100;
        private int bet = 5;
        private int playerone;
        private int playertwo;
        private bool playerturn = true;

        
        // Constructor for the Game class
        // This is the code that is called whenever an instance of Game is created
        public Game()
        {
            
        }
         
        // Function (segment of logic) called from Program.cs when the application starts
        public void Setup()
        {
            // Storing the String "My First Game" (without quotes) in the variable named "gameName"
            gameName = gameName + "Cee-lo";
        }
       

        public void Begin()
        {
            // Print various lines of strings to the Console window
            Console.WriteLine("");
            Console.WriteLine("Welcome to " + gameName);    // the + operator concatenate two strings (combines them)

            // Loop until shouldExit is true
            while (shouldExit == false)
            {
                Console.WriteLine("You have a balance of:" + balance);
                Console.Write("What to do: ");  // Write a partial line
                String command = Console.ReadLine();    // Read a line from Console (when Enter is hit) and store it in command

                HandleCommand(command);
            }
        }

        public void HandleCommand(String command)
        {
            // Enforce that the String is lowercase
            command = command.ToLower();

            if (command == "score")
            {
                Console.WriteLine("Your score is: " + score);
            }
            if (command == "roll")
            {
                balance = balance - bet;
                die = 1 + rand.Next(6);
                dietwo = 1 + rand.Next(6);
                diethree = 1 + rand.Next(6);
                Console.WriteLine("You rolled a: " + die + " You rolled a:  " + dietwo + " You rolled a: "
                    + diethree);
                if (die == dietwo)
                {
                    score = diethree;
                }
                if (die == diethree)
                {
                    score = dietwo;
                }
                if (dietwo == diethree)
                {
                    score = die;
                }

                if (die == 1 && dietwo == 2 && diethree == 3)
                {
                    score = 0;
                }
                if (die == 1 && dietwo == 3 && diethree == 2)
                {
                    score = 0;
                }
                if (die == 2 && dietwo == 1 && diethree == 3)
                {
                    score = 0;
                }
                if (die == 2 && dietwo == 3 && diethree == 1)
                {
                    score = 0;
                }
                if (die == 3 && dietwo == 1 && diethree == 2)
                {
                    score = 0;
                }
                if (die == 3 && dietwo == 2 && diethree == 1)
                {
                    score = 0;
                }
                if (die == 4 && dietwo == 5 && diethree == 6)
                {
                    score = 8;
                }
                if (die == 4 && dietwo == 6 && diethree == 5)
                {
                    score = 8;
                }
                if (die == 5 && dietwo == 4 && diethree == 6)
                {
                    score = 8;
                }
                if (die == 5 && dietwo == 6 && diethree == 4)
                {
                    score = 8;
                }
                if (die == 6 && dietwo == 4 && diethree == 5)
                {
                    score = 8;
                }
                if (die == 6 && dietwo == 5 && diethree == 4)
                {
                    score = 8;
                }
                  if (die == 1 && dietwo == 1 && diethree == 1)
                {
                    score = 7;
                }
                if (die == 2 && dietwo == 2 && diethree == 2)
                {
                    score = 7;
                }
                if (die == 3 && dietwo == 3 && diethree == 3)
                {
                    score = 7;
                }
                if (die == 4 && dietwo == 4 && diethree == 4)
                {
                    score = 7;
                }
                if (die == 5 && dietwo == 5 && diethree == 5)
                {
                    score = 7;
                }
                if (die == 6 && dietwo == 6 && diethree == 6)
                {
                    score = 7;
                }

                if (playerturn == true)
                {
                    playerone = score;
                    playerturn = false;
                }

                else
                {
                    playertwo = score;
                    playerturn = true;
                }

                if (playerone > playertwo)
                {
                    balance = balance + 5;
                }

                if (playerone < playertwo)
                {
                    balance = balance + 5;
                }

                if (playerone == playertwo)
                {
                    balance = balance + 0;
                }

                if (playerone == playertwo)
                {
                    balance = balance + 0;
                }
            }
        }
    }
}
